/**
 * Created by mcbird on 15/5/13.
 */
/* App Module */

var ENTRY_PROFILE = 'profile',
	ENTRY_MOVIES  = 'movies',
	ENTRY_CHAT    = 'chat',
	ENTRY_SETTING = 'setting';


/**
 * 左侧导航菜单
 * @type {module|*}
 */
var mainApp = angular.module('mainApp', [
	'ngRoute',
	'mainNavigatorControllers'
]);

mainApp.config(['$routeProvider',
	function($routeProvider) {
		$routeProvider.
			when('/:entryName', {
				templateUrl: '/partials/mainNavigator.html',
				controller: 'EntryListController'
			}).
			otherwise({
				redirectTo: '/' + ENTRY_PROFILE
			});
	}
]);


/**
 * 中间部分到模块入口
 * @type {module|*}
 */
//var entryApp = angular.module('entryApp', [
//	'ngRoute',
//	'entryControllers'
//]);
//
//entryApp.config(['$routeProvider',
//	function($routeProvider) {
//		$routeProvider.
//			when('/' + ENTRY_PROFILE, {
//				templateUrl: '/partials/entries/profile.html',
//				controller: 'profileRenderCtrl'
//			}).
//			when('/' + ENTRY_MOVIES, {
//				templateUrl: '/partials/entries/movies.html',
//				controller: 'moviesRenderCtrl'
//			}).
//			when('/' + ENTRY_CHAT, {
//				templateUrl: '/partials/entries/chat.html',
//				controller: 'chatRenderCtrl'
//			}).
//			when('/' + ENTRY_SETTING, {
//				templateUrl: '/partials/entries/setting.html',
//				controller: 'settingRenderCtrl'
//			}).
//			otherwise({
//				redirectTo: '/' + ENTRY_PROFILE
//			});
//	}
//]);


//var detailApp = angular.module('detailApp', [
//	'ngRoute',
//	'detailControllers'
//]);
//
//detailApp.config(['$routeProvider',
//	function($routeProvider) {
//		$routeProvider.
//			when('/' + ENTRY_PROFILE, {
//				templateUrl: '/partials/details/profile/activity.html',
//				controller: 'activityCtrl'
//			});
//	}
//]);
