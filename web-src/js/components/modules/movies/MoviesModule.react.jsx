var React = require('react');
var _ = require('lodash');
var classnames = require('classnames');

require('../../../../less/movie.less');

var ModuleStore = require('../../../stores/ModuleStore');
var ModuleActions = require('../../../actions/ModuleActions');
var Container = require('../common/ContainerModule.react');
var SearchBar = require('../common/SearchBarModule.react');
var NavBar = require('../common/NavBarModule.react');

var Glyphicon = require('react-bootstrap/lib/Glyphicon');

var movies = [
    {
        id: 1,
        title: '速度与激情',
        actors: ['憨大叔', '萌萝莉'],
        rate: 7.3,
    },
    {
        id: 2,
        title: '哆啦A梦：伴我同行',
        actors: ['水田山葵', '大原惠美', '关智一'],
        rate: 8.1,
    },
    {
        id: 3,
        title: 'BlaBla小魔仙',
        actors: ['憨大叔', '萌萝莉'],
        rate: 7.3,
    },
    {
        id: 4,
        title: 'BlaBla小魔仙',
        actors: ['憨大叔', '萌萝莉'],
        rate: 7.3,
    },
    {
        id: 5,
        title: '哆啦A梦：伴我同行',
        actors: ['水田山葵', '大原惠美', '关智一'],
        rate: 8.1,
    },
    {
        id: 6,
        title: '哆啦A梦：伴我同行',
        actors: ['水田山葵', '大原惠美', '关智一'],
        rate: 8.1,
    },
    {
        id: 7,
        title: '哆啦A梦：伴我同行',
        actors: ['水田山葵', '大原惠美', '关智一'],
        rate: 8.1,
    },
    {
        id: 8,
        title: '哆啦A梦：伴我同行',
        actors: ['水田山葵', '大原惠美', '关智一'],
        rate: 8.1,
    },
    {
        id: 9,
        title: '哆啦A梦：伴我同行',
        actors: ['水田山葵', '大原惠美', '关智一'],
        rate: 8.1,
    },
];

var MovieItem = React.createClass({
    render: function () {
        var stuffix = this.props.id % 2 === 0 ? '1' : '2';
        var coverUrl = 'url(/movies/cover_' + stuffix + '.jpg)';
        var coverStl = {
            backgroundImage: coverUrl
        };
        var actors = _.chain(this.props.actors).map(function (actor, key) {
            return key === 0 ? <span key={key}><a>{actor}</a></span> : <span key={key}>/<a>{actor}</a></span>;
        }).value();
        return (
            <div className='movie-item-wrap'>
                <div className='cover' style={coverStl}>
                    <div className='widget'>
                        <div className='play-btn text-center'><Glyphicon glyph='play' /></div>
                        <div className='love-btn'></div>
                        <div className='share-btn'></div>
                    </div>
                    <div className='brand'>{this.props.rate}</div>
                </div>
                <h3 className='title'>{this.props.title}</h3>
                <p className='desc-text'>{actors}</p>
            </div>
        );
    }
});

module.exports = React.createClass({
    getInitialState: function () {
        return {
            movies: movies
        };
    },
    render: function(){
        var Movies = _.chain(this.state.movies).map(function (movie) {
            return (
                <MovieItem key={movie.id} id={movie.id} title={movie.title} actors={movie.actors} rate={movie.rate}></MovieItem>
            );
        }).value();
        return (
            <Container>
                <SearchBar/>
                <div className="movies-container">
                    <NavBar/>
                    {Movies}
                </div>
            </Container>
        );
    }
});